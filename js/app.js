var app = angular.module('imageViewer', ['ng']);

  app.controller('AppGallery', ['$scope','$http', function AppGallery($scope, $http) {
  $http.jsonp('https://api.flickr.com/services/feeds/photos_public.gne?tags=potato&tagmode=all&format=json').success(function (data){
  });
  
    jsonFlickrFeed = function (data){
      $scope.flickrData = data;
    };

  }]);

  app.filter('justTheAuthorPlease', function () {
    return function (value) {
        return value.slice(19, -1);
    };
  });