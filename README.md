Potato Front End Exercise
=========================

Interpreted Client Spec
-----------------------
- Single page app
- Wireframes provided for design (allows for some personalisation)
- Broswer support: Chrome, Safari, Firefox, IE10+
- Responsive design
- Built in JS
- Uses AngularJS

**Extras**

- Search functionality
- Infinite scrolling (auto-load more results on scroll)
- Git history
- TDD